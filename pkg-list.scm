;; Copyright (C) 2010, 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the MIT/X11 license.

;; You should have received a copy of the MIT/X11 license along with
;; this program. If not, see
;; <http://www.opensource.org/licenses/mit-license.php>.

(package (wak-syn-param (0) (2007 7 21) (1))
  (depends (wak-common))
  
  (synopsis "operators with extended parameter syntax")
  (description
   "This package provides a form that allows to create macros"
   "that accept arguments by position or by a named pattern.")
  
  (libraries
   (sls -> "wak")
   (("syn-param" "private") -> ("wak" "syn-param" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
